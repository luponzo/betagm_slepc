/*
 * model_with_weighted_interactions.h
 *
 *  Created on: Jun 29, 2012
 *      Author: gpolles
 */

#ifndef MODEL_WITH_WEIGHTED_INTERACTIONS_H_
#define MODEL_WITH_WEIGHTED_INTERACTIONS_H_

#include "SparseMatrix.h"

void construct_Matrix_M(betagm::SparseMatrix& m,
    struct residue *residues,
    int prot_len,
    struct params* parameters);
void construct_contact_map(int **cmap, struct residue *residues, int prot_len, struct params* parameters);
void construct_cbetas(struct residue *residues, int prot_len);

#endif /* MODEL_WITH_WEIGHTED_INTERACTIONS_H_ */
