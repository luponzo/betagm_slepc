#include <cmath>
#include <cstdio>
#include <cstdlib>

#include "definitions.h"
#include "model_with_weighted_interactions.h"
#include "io.h"

#include "SparseMatrix.h"

void construct_Matrix_M(betagm::SparseMatrix& m, struct residue *residues, int prot_len, struct params* params){
#define weight(x) ( ((x) < params->Int_Range) ? 1 : exp((params->Int_Range-x)/2.0))


  const double intraMolMult = 5.0;
  // to minimize reallocation and optimize performance
  // reserve space on the sparse matrix
  for (size_t i = 0; i < m.getSize(); ++i) {
    // six neighbors should suffice?
    m.getColumn(i).reserve(4);
  }


  double d, dvec[3], temp, di[3],dj[3],normj =0 ,normi =0;
  int i,j;
  int mu, nu;
  double c_i,c_ip,c_im,c_j,c_jp,c_jm, x;


  /* Now we construct the matrix of effective quadratic interactions
     among CA's.
     The Hamiltonian is given by

     H = 1/2 sum_{i,j} sum{mu,nu} x^CA_{i,mu} M_{i,j,mu,nu} x^CA_{j,mu}

     where i and j denote the sites, mu,nu run over cartesian components,
     and M_{i,j,mu,nu} is derived from the expansion around the minimum of the
     interaction potential, V( r_{ij}):

     M_{i,j,mu,nu} = 1/2 V'' (r0_{ij}) .


     In this programme M_{i,j,mu,nu} is stored in a two-dimenzional matrix as

     M[i+prot_len*mu][j+prot_len*nu].
  */

  int currentChainNo = 0;

  /* We begin by including covalently-bonded interactions */
  printf("\n  >  Bonded Interactions\n");
  for(i=0; i < (prot_len-1); ++i){
    if(i%100 == 0) printf("\r index: %7d",i);
    residues[i].chainNo = currentChainNo;
    j= i+1;
    d = dist_d(residues[i].ca,residues[j].ca,3);
    if ( d > BROKEN_CHAIN_CUTOFF) {
      ++currentChainNo;
      continue;
    }
    /* Let's build the distance vector */
    for(mu=0; mu < 3; mu++){
      dvec[mu]= residues[i].ca[mu]-residues[j].ca[mu];
    }

    for (mu=0; mu < 3; mu++){
      for (nu=0; nu < 3; nu++){
        temp = params->V_PEPT*dvec[mu]*dvec[nu]/(d*d);
        m(i+prot_len*mu , i+prot_len*nu) += temp;
        m(i+prot_len*mu , j+prot_len*nu) += -temp;
        m(j+prot_len*mu , i+prot_len*nu) += -temp;
        m(j+prot_len*mu , j+prot_len*nu) += temp;
      }
    }
  }


  /* Include contact interactions CA(i)-CA(j)*/
  printf("\n  >  CA CA\n");
  for(i=0; i < prot_len; ++i){
    if(i%10 == 0) printf("\r index: %7d of %7d",i,prot_len);
    for(j=0; j < prot_len; ++j){
      if (j==i) continue;




      d = dist_d(residues[i].ca,residues[j].ca,3);
      if (d > params->Int_Range) continue;
      // if consecutive, consider non covalent interaction
      // only if they are not covalently bonded
      if (abs(j-i)==1 && d < BROKEN_CHAIN_CUTOFF) continue;
      /* Let's build the distance vector */
      for(mu=0; mu < 3; mu++){
        dvec[mu]= residues[i].ca[mu]-residues[j].ca[mu];
      }

      for (mu=0; mu < 3; mu++){
        for (nu=0; nu < 3; nu++){
          if(residues[i].chainNo == residues[j].chainNo){
            temp = 0.5*params->V_CACA*dvec[mu]*dvec[nu]/(d*d)*weight(d)*intraMolMult;
          }else{
            temp = 0.5*params->V_CACA*dvec[mu]*dvec[nu]/(d*d)*weight(d);
          }

          m(i+prot_len*mu , i+prot_len*nu) += temp;
          m(i+prot_len*mu , j+prot_len*nu) += -temp;
          m(j+prot_len*mu , i+prot_len*nu) += -temp;
          m(j+prot_len*mu , j+prot_len*nu) += temp;
        }
      }
    }
  }

  /* Include contact interactions CA(i)-CB(j)*/
  printf("\n  >  CA CB\n");
  for(i=0; i < prot_len; ++i){
    if(i%10 == 0) printf("\r index: %7d of %7d",i,prot_len);
    for(j=0; j < prot_len; ++j){
      if (j==i) continue;
      if (residues[j].No_CB==1) continue;
      d = dist_d(residues[i].ca,residues[j].cb,3);
      if (d > params->Int_Range) continue;

      /* Let's build the distance vector */
      for(mu=0; mu < 3; mu++){
        dvec[mu]= residues[i].ca[mu]-residues[j].cb[mu];
      }

      if (residues[j].CB_equal_CA==0){
        for(mu=0; mu < 3; mu++){
          dj[mu]= 2*residues[j].ca[mu]-residues[j+1].ca[mu]-residues[j-1].ca[mu];
        }
        normj=norm_d(dj,3);
      }

      for (mu=0; mu < 3; mu++){
        for (nu=0; nu < 3; nu++){

          /* No factor of 1/2 since  V_CA(i)CB(j) is different from V_CA(j)CB(i) */
          temp = params->V_CACB*dvec[mu]*dvec[nu]/(d*d)*weight(d);

          /* To calculate the constribution to the matrix M of the
             interaction between CA(i) and CB(j), we need to recast
             [CA(i) - CB(j)] in terms of CA coordinates only. */

          /* Projection of CA(i) on CA(i) [ equal to 1!!] */
          c_i= 1.0;

          /* ... and now let's calculate the projection of -CB(j) on
             CA(j)   -> c_j
             CA(j+1) -> c_jp
             CA(j-1) -> c_jm
          */

          if (residues[j].CB_equal_CA==1){
            c_j= -1.0;
            c_jp= 0.0;
            c_jm= 0.0;
          }
          else{
            c_j= -(1+2*3.0/normj);
            c_jp= (3.0/normj);
            c_jm= (3.0/normj);
          }

          x= c_i*c_i*temp;  if (fabs(x) > 1.0e-10) m(i+prot_len*mu , i+prot_len*nu) += x;
          x= c_i*c_j*temp;  if (fabs(x) > 1.0e-10) m(i+prot_len*mu , j+prot_len*nu) += x;
          x= c_i*c_jp*temp; if (fabs(x) > 1.0e-10) m(i+prot_len*mu , j+1+prot_len*nu) += x;
          x= c_i*c_jm*temp; if (fabs(x) > 1.0e-10) m(i+prot_len*mu , j-1+prot_len*nu) += x;


          x= c_j*c_i*temp;  if (fabs(x) > 1.0e-10) m(j+prot_len*mu , i+prot_len*nu) += x;
          x= c_j*c_j*temp;  if (fabs(x) > 1.0e-10) m(j+prot_len*mu , j+prot_len*nu) += x;
          x= c_j*c_jp*temp; if (fabs(x) > 1.0e-10) m(j+prot_len*mu , j+1+prot_len*nu) += x;
          x= c_j*c_jm*temp; if (fabs(x) > 1.0e-10) m(j+prot_len*mu , j-1+prot_len*nu) += x;

          x= c_jp*c_i*temp;  if (fabs(x) > 1.0e-10) m(j+1+prot_len*mu , i+prot_len*nu) += x;
          x= c_jp*c_j*temp;  if (fabs(x) > 1.0e-10) m(j+1+prot_len*mu , j+prot_len*nu) += x;
          x= c_jp*c_jp*temp; if (fabs(x) > 1.0e-10) m(j+1+prot_len*mu , j+1+prot_len*nu) += x;
          x= c_jp*c_jm*temp; if (fabs(x) > 1.0e-10) m(j+1+prot_len*mu , j-1+prot_len*nu) += x;

          x= c_jm*c_i*temp;  if (fabs(x) > 1.0e-10) m(j-1+prot_len*mu , i+prot_len*nu) += x;
          x= c_jm*c_j*temp;  if (fabs(x) > 1.0e-10) m(j-1+prot_len*mu , j+prot_len*nu) += x;
          x= c_jm*c_jp*temp; if (fabs(x) > 1.0e-10) m(j-1+prot_len*mu , j+1+prot_len*nu) += x;
          x= c_jm*c_jm*temp; if (fabs(x) > 1.0e-10) m(j-1+prot_len*mu , j-1+prot_len*nu) += x;
        }
      }


    }
  }


  /* Include contact interactions CB(i)-CB(j)*/
  printf("\n  >  CB CB\n");
  for(i=0; i < prot_len; ++i){
    if(i%10 == 0) printf("\r index: %7d of %7d",i,prot_len);
    if (residues[i].No_CB==1) continue;
    for(j=0; j < prot_len; ++j){
      if (j==i) continue;
      if (residues[j].No_CB==1) continue;
      d = dist_d(residues[i].cb,residues[j].cb,3);
      if (d > params->Int_Range) continue;

      /* Let's build the distance vector */
      for(mu=0; mu < 3; mu++){
        dvec[mu]= residues[i].cb[mu]-residues[j].cb[mu];
      }

      if (residues[i].CB_equal_CA==0){
        for(mu=0; mu < 3; mu++){
          di[mu]= 2*residues[i].ca[mu]-residues[i+1].ca[mu]-residues[i-1].ca[mu];
        }
        normi=norm_d(di,3);
      }

      if (residues[j].CB_equal_CA==0){
        for(mu=0; mu < 3; mu++){
          dj[mu]= 2*residues[j].ca[mu]-residues[j+1].ca[mu]-residues[j-1].ca[mu];
        }
        normj=norm_d(dj,3);
      }

       for (mu=0; mu < 3; mu++){
        for (nu=0; nu < 3; nu++){
          temp = 0.5*params->V_CBCB*dvec[mu]*dvec[nu]/(d*d)*weight(d);

          /* To calculate the constribution to the matrix M of the
             interaction between CA(i) and CB(j), we need to recast
             [CA(i) - CB(j)] in terms of CA coordinates only. */

         /* Let's calculate the projection of CB(i) on CA(i), CA(i+1) and CA(i-1) */
          if (residues[i].CB_equal_CA==1){
            c_i= 1.0;
            c_ip= 0.0;
            c_im= 0.0;
          }
          else{
            c_i= (1+2*3.0/normi);
            c_ip= -(3.0/normi);
            c_im= -(3.0/normi);
          }

         /* ... and now let's calculate the projection of -CB(j) on CA(j), CA(j+1) and CA(j-1) */
          if (residues[j].CB_equal_CA==1){
            c_j= -1.0;
            c_jp= 0.0;
            c_jm= 0.0;
          }
          else{
            c_j= -(1+2*3.0/normj);
            c_jp= (3.0/normj);
            c_jm= (3.0/normj);
          }

          x= c_i*c_i*temp;  if (fabs(x) > 1.0e-10) m(i+prot_len*mu , i+prot_len*nu)  += x;
          x= c_i*c_ip*temp; if (fabs(x) > 1.0e-10)m(i+prot_len*mu , i+1+prot_len*nu) += x;
          x= c_i*c_im*temp; if (fabs(x) > 1.0e-10)m(i+prot_len*mu , i-1+prot_len*nu) += x;
          x= c_i*c_j*temp;  if (fabs(x) > 1.0e-10)m(i+prot_len*mu , j+prot_len*nu)   += x;
          x= c_i*c_jp*temp; if (fabs(x) > 1.0e-10)m(i+prot_len*mu , j+1+prot_len*nu) += x;
          x= c_i*c_jm*temp; if (fabs(x) > 1.0e-10)m(i+prot_len*mu , j-1+prot_len*nu) += x;

          x= c_ip*c_i*temp;  if (fabs(x) > 1.0e-10) m(i+1+prot_len*mu , i+prot_len*nu)  += x;
          x= c_ip*c_ip*temp; if (fabs(x) > 1.0e-10)m(i+1+prot_len*mu , i+1+prot_len*nu) += x;
          x= c_ip*c_im*temp; if (fabs(x) > 1.0e-10)m(i+1+prot_len*mu , i-1+prot_len*nu) += x;
          x= c_ip*c_j*temp;  if (fabs(x) > 1.0e-10)m(i+1+prot_len*mu , j+prot_len*nu)   += x;
          x= c_ip*c_jp*temp; if (fabs(x) > 1.0e-10)m(i+1+prot_len*mu , j+1+prot_len*nu) += x;
          x= c_ip*c_jm*temp; if (fabs(x) > 1.0e-10)m(i+1+prot_len*mu , j-1+prot_len*nu) += x;

          x= c_im*c_i*temp;  if (fabs(x) > 1.0e-10) m(i-1+prot_len*mu , i+prot_len*nu)  += x;
          x= c_im*c_ip*temp; if (fabs(x) > 1.0e-10)m(i-1+prot_len*mu , i+1+prot_len*nu) += x;
          x= c_im*c_im*temp; if (fabs(x) > 1.0e-10)m(i-1+prot_len*mu , i-1+prot_len*nu) += x;
          x= c_im*c_j*temp;  if (fabs(x) > 1.0e-10)m(i-1+prot_len*mu , j+prot_len*nu)   += x;
          x= c_im*c_jp*temp; if (fabs(x) > 1.0e-10)m(i-1+prot_len*mu , j+1+prot_len*nu) += x;
          x= c_im*c_jm*temp; if (fabs(x) > 1.0e-10)m(i-1+prot_len*mu , j-1+prot_len*nu) += x;

          x= c_j*c_i*temp;  if (fabs(x) > 1.0e-10) m(j+prot_len*mu , i+prot_len*nu)  += x;
          x= c_j*c_ip*temp; if (fabs(x) > 1.0e-10)m(j+prot_len*mu , i+1+prot_len*nu) += x;
          x= c_j*c_im*temp; if (fabs(x) > 1.0e-10)m(j+prot_len*mu , i-1+prot_len*nu) += x;
          x= c_j*c_j*temp;  if (fabs(x) > 1.0e-10)m(j+prot_len*mu , j+prot_len*nu)   += x;
          x= c_j*c_jp*temp; if (fabs(x) > 1.0e-10)m(j+prot_len*mu , j+1+prot_len*nu) += x;
          x= c_j*c_jm*temp; if (fabs(x) > 1.0e-10)m(j+prot_len*mu , j-1+prot_len*nu) += x;

          x= c_jp*c_i*temp;  if (fabs(x) > 1.0e-10) m(j+1+prot_len*mu , i+prot_len*nu)  += x;
          x= c_jp*c_ip*temp; if (fabs(x) > 1.0e-10)m(j+1+prot_len*mu , i+1+prot_len*nu) += x;
          x= c_jp*c_im*temp; if (fabs(x) > 1.0e-10)m(j+1+prot_len*mu , i-1+prot_len*nu) += x;
          x= c_jp*c_j*temp;  if (fabs(x) > 1.0e-10)m(j+1+prot_len*mu , j+prot_len*nu)   += x;
          x= c_jp*c_jp*temp; if (fabs(x) > 1.0e-10)m(j+1+prot_len*mu , j+1+prot_len*nu) += x;
          x= c_jp*c_jm*temp; if (fabs(x) > 1.0e-10)m(j+1+prot_len*mu , j-1+prot_len*nu) += x;

          x= c_jm*c_i*temp;  if (fabs(x) > 1.0e-10) m(j-1+prot_len*mu , i+prot_len*nu)  += x;
          x= c_jm*c_ip*temp; if (fabs(x) > 1.0e-10)m(j-1+prot_len*mu , i+1+prot_len*nu) += x;
          x= c_jm*c_im*temp; if (fabs(x) > 1.0e-10)m(j-1+prot_len*mu , i-1+prot_len*nu) += x;
          x= c_jm*c_j*temp;  if (fabs(x) > 1.0e-10)m(j-1+prot_len*mu , j+prot_len*nu)   += x;
          x= c_jm*c_jp*temp; if (fabs(x) > 1.0e-10)m(j-1+prot_len*mu , j+1+prot_len*nu) += x;
          x= c_jm*c_jm*temp; if (fabs(x) > 1.0e-10)m(j-1+prot_len*mu , j-1+prot_len*nu) += x;

        }
       }
    }
  }

  printf("\nEnd construction of matrix with effective quadratic interactions among CA's\n");

}


void construct_cbetas(struct residue *residues, int prot_len){

  double x[3],d;
  int i,j;

   for(i=0; i < prot_len; i++){
     /* Omit GLY's from CB construction */
     if (residues[i].residue_type==0) residues[i].No_CB=1;
     else {
       residues[i].No_CB=0;
       residues[i].CB_equal_CA=0;
     }
   }

   /* for non-gly residues at termini of the peptide chain(s) CB coincides with CA  */

   if (residues[0].No_CB==0){
     residues[0].CB_equal_CA=1; 
     for(j=0; j < 3; j++){ 
       residues[0].cb[j]=residues[0].ca[j];
     }
   }

   if (residues[prot_len-1].No_CB==0){
     residues[prot_len-1].CB_equal_CA=1; 
     for(j=0; j < 3; j++){ 
       residues[prot_len-1].cb[j]=residues[prot_len-1].ca[j];
     }
   }

   /* let's detect the presence of other chain termini */
   for(i=0; i < (prot_len-1); i++){
     d = dist_d(residues[i].ca,residues[i+1].ca,3);

     if (d > BROKEN_CHAIN_CUTOFF) {
       if (residues[i].No_CB==0){
         residues[i].CB_equal_CA=1;
         
         for(j=0; j < 3; j++){ 
           residues[i].cb[j]=residues[i].ca[j];
         }
       }
       if (residues[i+1].No_CB==0){
         residues[i+1].CB_equal_CA=1;
         for(j=0; j < 3; j++){ 
           residues[i+1].cb[j]=residues[i+1].ca[j];
         }
       }

     }
   }

   for(i=0; i < prot_len; i++){ 
     if ((residues[i].No_CB==0) && (residues[i].CB_equal_CA==0)){
       /* Apply simplified Park-Levitt construction for CB */
       for(j=0; j < 3; j++){
         x[j] = 2*residues[i].ca[j] - residues[i-1].ca[j] - residues[i+1].ca[j];
       }
       for(j=0; j < 3; j++){
         residues[i].cb[j] = residues[i].ca[j] + 3.0*x[j]/norm_d(x,3);
       }
     }

   }
}

/******************************************/

void construct_contact_map(int **cmap, struct residue *residues, int prot_len, struct params* params){

  double d;
  int i,j;

  for(i=0; i < prot_len; i++){
    for(j= 0; j < prot_len; j++){
      cmap[i][j]=0;
    }
  }
  
  for(i=0; i < prot_len; i++){
    for(j= i+1; j < prot_len; j++){
      d = dist_d(residues[i].ca,residues[j].ca,3);

      /* if i anf j are consecutive consider their contact (non-covalent) interaction
         only if they are non-covalently bonded and yet sufficiently close (i.e. if their separation is
         greaten than BROKEN_CHAIN_CUTOFF and smaller than Int_Range) */

      if (abs(j-i)==1){
        if (d > BROKEN_CHAIN_CUTOFF){
          printf("Found broken peptide chain at residue %3d\n",i+1);
          if  ( d < params->Int_Range) cmap[i][j]=cmap[j][i]=1;
        }
      } 
      /* For all other a.a. pairs check if they are closer than Int_Range */
      else if (d < params->Int_Range){
        cmap[i][j]=cmap[j][i]=1;
      } 
      
    }
  }

  printf("End construction of effective CB's\n");
}

/******************************************/
