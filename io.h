/*
 * io.h
 *
 *  Created on: Jun 29, 2012
 *      Author: gpolles
 */

#ifndef IO_H_
#define IO_H_

FILE* open_file_w (char *name);
FILE* open_file_r (char *name);
FILE* open_file_a (char *name);
void close_file (FILE * fp);
void read_pdb_file(char *file_name,struct residue residues[], int *prot_len);
void screen_dump_prot_details(struct residue *residues, int prot_len);
void read_parameters(struct params* parameters);
void dump_eigenvalues(double *eigenvalues, int prot_len, char *pdbname);
void dump_top_vectors(double **eigenvectors, int prot_len, int ntop, char *pdbname);
void dump_full_covariance_matrix(double **invm, int prot_len, char *pdbname);
void dump_reduced_covariance_matrix(double **invm, int prot_len, char *pdbname);
void dump_normalised_reduced_covariance_matrix(double **invm, int prot_len, char *pdbname);
void dump_mean_square_displacements(double **invm, int prot_len, char *pdbname);


#endif /* IO_H_ */
