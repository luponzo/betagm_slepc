all: 
	g++ -O3 -Wl,-rpath,${PETSC_DIR}/${PETSC_ARCH}/lib,-rpath,${SLEPC_DIR}/${SLEPC_ARCH}/lib -I${PETSC_DIR}/${PETSC_ARCH}/include -I${PETSC_DIR}/include -I${SLEPC_DIR}/${SLEPC_ARCH}/include -I${SLEPC_DIR}/include -L${PETSC_DIR}/${PETSC_ARCH}/lib -L${SLEPC_DIR}/${SLEPC_ARCH}/lib -lslepc -lpetsc betaGM_lowmem_slepc.cpp Clock.cpp io.c model_with_weighted_interactions.cpp my_malloc.c SparseMatrix.cpp SymmetricSparseMatrix.cpp -lslepc -lpetsc -o betaGM_lowmem_slepc
 
