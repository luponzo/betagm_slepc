#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <malloc.h>
#include <string.h>



#include "definitions.h"
#include "io.h"
/**********/
FILE *
open_file_w (char *name)
{

  FILE *fp;
  FILE *fe;

  if ((fp = fopen (name, "w")) == NULL)
    {
      fprintf (stderr,"Error. Could not open file %s.\n.Exiting.\n", name);
      fe=fopen("error.dat","w");
      fprintf (fe,"Error. Could not open file %s.\n.Exiting.\n", name);
      fclose(fe);
      exit(1);
    }
  return (fp);
}

/**********/
FILE *
open_file_r (char *name)
{

  FILE *fp;
  FILE *fe;

  if ((fp = fopen (name, "r")) == NULL)
    {
      fprintf (stderr,"Error. Could not open file %s.\n.Exiting.\n", name);
      fe=open_file_w("error.dat");
      fprintf (fe,"Error. Could not open file %s.\n.Exiting.\n", name);
      fclose(fe);
      exit(1);
    }
  return (fp);
}
/**********/
FILE *
open_file_a (char *name)
{

  FILE *fp;
  FILE *fe;

  if ((fp = fopen (name, "a")) == NULL)
    {
      fprintf (stderr,"Error. Could not open file %s.\n.Exiting.\n", name);
      fe=open_file_w("error.dat");
      fprintf (fe,"Error. Could not open file %s.\n.Exiting.\n", name);
      fclose(fe);
      exit(1);
    }
  return (fp);
}

/**********/
void 
close_file (FILE * fp)
{
  fclose (fp);
}

/**********/

void read_pdb_file(char *file_name,struct residue residues[], int *prot_len){


  int i;
  char line[200], field[200], *a;
  FILE *fp, *fe;
  int  discard_alternative_position;

  /* PDB FORMAT 

COLUMNS        DATA TYPE     FIELD        DEFINITION 
------------------------------------------------------------------------------------- 
 1 -  6        Record name   "ATOM  " 
 
 7 - 11        Integer       serial       Atom serial number. 
 
13 - 16        Atom          name         Atom name. 
 
17             Character     altLoc       Alternate location indicator. 
 
18 - 20        Residue name  resName      Residue name. 
 
22             Character     chainID      Chain identifier. 
 
23 - 26        Integer       resSeq       Residue sequence number. 
 
27             AChar         iCode        Code for insertion of residues. 
 
31 - 38        Real(8.3)     x            Orthogonal coordinates for X in Angstroms. 
 
39 - 46        Real(8.3)     y            Orthogonal coordinates for Y in Angstroms. 
 
47 - 54        Real(8.3)     z            Orthogonal coordinates for Z in Angstroms. 
 
55 - 60        Real(6.2)    occupancy     Occupancy. 
 
61 - 66        Real(6.2)    tempFactor    Temperature factor. 
  
77 - 78        LString(2)   element       Element symbol, right-justified. 
 
79 - 80        LString(2)   charge        Charge on the atom. 

  */

  /* Find out length of protein */

  fp = open_file_r(file_name);


  *prot_len=0;
  while (fgets(line, 200, fp)){
    if (strncmp (line, "ENDMDL", 6)==0){
      printf("Stopping at end of first model.\n");
      break;
    }
    /* check if line buffer begins with "ATOM" */
    if (!strncmp (line, "ATOM", 4)){
      a = line +12; /* advance line buffer and check if 
                       we have a CA atom */
      /* check if character at position 16 is different from ' '
         or 'A'. If so, we are facing an alternative position
         and we will not consider it */
      discard_alternative_position=1;
      if (line[16]==' ') discard_alternative_position=0;
      if (line[16]=='A') discard_alternative_position=0;

      if ((!strncmp (a, " CA ", 4)) && (discard_alternative_position==0)){
        (*prot_len)++;
      }
    }
  }
  rewind(fp);
  if (*prot_len==0){
    fprintf(stderr,"Fatal nput error! Found no CA atoms.\nPlease check input file for compliance with PDB formatting. For example, ATOM and CA strings should start at columns 1 and 13, respectively.\n");
    fe = open_file_w("error.dat");
    fprintf(fe,"Fatal nput error! Found no CA atoms.\nPlease check input file for compliance with PDB formatting. For example, ATOM and CA strings should start at columns 1 and 13, respectively.\n");
    fclose (fe);
    exit(1);
  }
  printf("Reading coordinates of the %4d CA atoms\n",*prot_len);
  
  /* Let's allocate the memory for the protein */
  
  

  i=0;
  while (fgets(line, 200, fp)){
    if (strncmp (line, "ENDMDL", 6)==0){
      printf("Stopping at end of first model.\n");
      break;
    }
    /* check if line buffer begins with "ATOM" */
    if (!strncmp (line, "ATOM", 4)){
      a = line +12; 
                        /* advance line buffer and check if 
                       we have a CA atom */
      /* check if character at position 16 is different from ' '
         or 'A'. If so, we are facing an alternative position
         and we will not consider it */
      discard_alternative_position=1;
      if (line[16]==' ') discard_alternative_position=0;
      if (line[16]=='A') discard_alternative_position=0;
      if (discard_alternative_position==1) continue;

      if (!strncmp (a, " CA ", 4)){
        a = line +17;

        if      (!strncmp (a, "LEU",3))  residues[i].residue_type = 3;
        else if (!strncmp (a, "ALA",3))  residues[i].residue_type = 1;
        else if (!strncmp (a, "VAL",3))  residues[i].residue_type = 2;
        else if (!strncmp (a, "GLY",3))  residues[i].residue_type = 0;
        else if (!strncmp (a, "ILE",3))  residues[i].residue_type = 4;
        else if (!strncmp (a, "SER",3))  residues[i].residue_type = 12;
        else if (!strncmp (a, "THR",3))  residues[i].residue_type = 13;
        else if (!strncmp (a, "CYS",3))  residues[i].residue_type = 5;
        else if (!strncmp (a, "MET",3))  residues[i].residue_type = 6;
        else if (!strncmp (a, "PRO",3))  residues[i].residue_type = 8;
        else if (!strncmp (a, "ASP",3))  residues[i].residue_type = 16;
        else if (!strncmp (a, "ASN",3))  residues[i].residue_type = 17;
        else if (!strncmp (a, "GLU",3))  residues[i].residue_type = 18;
        else if (!strncmp (a, "GLN",3))  residues[i].residue_type = 19;
        else if (!strncmp (a, "LYS",3))  residues[i].residue_type = 14;
        else if (!strncmp (a, "ARG",3))  residues[i].residue_type = 15;
        else if (!strncmp (a, "HIS",3))  residues[i].residue_type = 10;
        else if (!strncmp (a, "PHE",3))  residues[i].residue_type = 7;
        else if (!strncmp (a, "TYR",3))  residues[i].residue_type = 9;
        else if (!strncmp (a, "TRP",3))  residues[i].residue_type = 11;
        else
	    {
            
	      residues[i].residue_type = 20;
              printf ("WARNING. Found unknown residue type: (%s)\n", a );
	    }


        /* Read CA coordinates */
        a=line+30;
        strncpy(field,a,8);field[8]='\0';
        if (sscanf(field,"%lf",&residues[i].ca[0])!=1) {
        fprintf(stderr,"Fatal error. Missing coordinate for %dth amino acid in protein %s.\n",i, file_name);
            fe = open_file_w("error.dat");
            fprintf(fe,"Fatal error. Missing coordinate for %dth amino acid in protein %s.\n",i, file_name);
            fclose (fe);
        exit(1);
        }
        a=line+38;
        strncpy(field,a,8);field[8]='\0';
        if (sscanf(field,"%lf",&residues[i].ca[1])!=1) {
           fprintf(stderr,"Fatal error. Missing coordinate for %dth amino acid in protein %s.\n",i, file_name); 
           fe = open_file_w("error.dat"); 
           fprintf(fe,"Fatal error. Missing coordinate for %dth amino acid in protein %s.\n",i, file_name); 
          fclose(fe);
           exit(1);
        }
        a=line+46;
        strncpy(field,a,8);field[8]='\0';
        if (sscanf(field,"%lf",&residues[i].ca[2])!=1) {
        fprintf(stderr,"Fatal error. Missing coordinate for %dth amino acid in protein %s.\n",i, file_name);
           fe = open_file_w("error.dat");
        fprintf(fe,"Fatal error. Missing coordinate for %dth amino acid in protein %s.\n",i, file_name);
           fclose(fe); 
        exit(1);}

        /* Finally read B factor */
        a= line + 60;
        strncpy(field,a,6);field[6]='\0';
        if (sscanf(field,"%lf",&residues[i].Bfactor)!=1) {residues[i].Bfactor=0.0;}
        i++;
      }
    }
  }
  fclose(fp);
  
  if (i != *prot_len){
    fprintf(stderr,"Error. Inconsistent number of CA atoms between first and second pass of pdb file (%d vs %d).\nThis should not have happened! Please report bug to C. Micheletti, michelet@sissa.it. Thank you!\n",
           i,*prot_len);

           fe = open_file_w("error.dat");
    fprintf(fe,"Error. Inconsistent number of CA atoms between first and second pass of pdb file (%d vs %d).\nThis should not have happened! Please report bug to C. Micheletti, michelet@sissa.it. Thank you!\n",
           i,*prot_len);
           fclose(fe);
    exit(1);
  }



}



/**********************************/

void screen_dump_prot_details(struct residue *residues, int prot_len){
  /* dump on the screen the CA coordinates, a.a. type and B factors */
  
  int i;

  for(i=0; i < prot_len; i++){
    printf("%4d type: %2d  %9.5lf %9.5lf %9.5lf  Bfact: %6.3lf\n",
           i,
           residues[i].residue_type,
           residues[i].ca[0],residues[i].ca[1],residues[i].ca[2],
           residues[i].Bfactor);
  }
  


}

/**********************************/

void read_parameters(struct params* params){

  FILE *fp;
  char string[100];

  fp = open_file_r("PARAMS_BETAGM.DAT");
  fscanf(fp,"%s %lf",string,&params->Int_Range);
  fscanf(fp,"%s %lf",string,&params->V_PEPT);
  fscanf(fp,"%s %lf",string,&params->V_CACA);
  fscanf(fp,"%s %lf",string,&params->V_CACB);
  fscanf(fp,"%s %lf",string,&params->V_CBCB);
  fscanf(fp,"%s %d",string,&params->DUMP_EIGENVALUES);
  fscanf(fp,"%s %d",string,&params->N_SLOWEST_EIGENVECT);
  fscanf(fp,"%s %d",string,&params->DUMP_FULL_COVMAT);
  fscanf(fp,"%s %d",string,&params->DUMP_REDUCED_COVMAT);
  fscanf(fp,"%s %d",string,&params->DUMP_NORMALISED_REDUCED_COVMAT);
  fscanf(fp,"%s %d",string,&params->DUMP_MEAN_SQUARE_DISPL);
  params->cgfactor = 0;
  fscanf(fp,"%s %d",string,&params->cgfactor);
  if(!params->cgfactor) params->cgfactor = 1;

  fclose(fp);

  printf("MODEL PARAMETERS: \n");
  printf("\tStrength of peptide bond:              %lf\n",params->V_PEPT);
  printf("\tContact Interaction Range:             %lf\n",params->Int_Range);
  printf("\tStrength of non-bonded CA-CA contacts: %lf\n",params->V_CACA);
  printf("\tStrength of non-bonded CA-CB contacts: %lf\n",params->V_CACB);
  printf("\tStrength of non-bonded CB-CB contacts: %lf\n",params->V_CBCB);
  printf("\n");
  printf("\tDUMP_EIGENVALUES:               "); if (params->DUMP_EIGENVALUES==1) printf("YES\n"); else printf("NO\n");
  if(params->N_SLOWEST_EIGENVECT>0) 
    printf("\tPRINT TOP %4d SLOWEST EIGENVECTORS\n",params->N_SLOWEST_EIGENVECT);
  else 
    printf("\tALL EIGENVECTORS WILL BE PRINTED\n");
  printf("\tDUMP_FULL_COVMAT:               "); if (params->DUMP_FULL_COVMAT==1) printf("YES\n"); else printf("NO\n");
  printf("\tDUMP_REDUCED_COVMAT:            "); if (params->DUMP_REDUCED_COVMAT==1) printf("YES\n"); else printf("NO\n");
  printf("\tDUMP_NORMALISED_REDUCED_COVMAT: "); if (params->DUMP_NORMALISED_REDUCED_COVMAT==1) printf("YES\n"); else printf("NO\n");
  printf("\tDUMP_MEAN_SQUARE_DISPL:         "); if (params->DUMP_MEAN_SQUARE_DISPL==1) printf("YES\n"); else printf("NO\n");
  printf("\tcgFACTOR:                        %d\n", params->cgfactor);
  printf("-------\n\n");
}
/**********/


/******************************************/
void dump_eigenvalues(double *eigenvalues, int prot_len, char *pdbname){

  int i;
  char filename[200];
  FILE *fp;

  sprintf(filename,"%s_eigenvalues.dat",pdbname);

  fp =open_file_w(filename);

  for(i=0; i < 3*prot_len; i++){
    fprintf(fp,"%4d %e\n",i,eigenvalues[3*prot_len-1-i]);
  }

  fclose(fp);
  printf("Eigenvalues written to file %s.\n",filename);

}

/******************************************/
void dump_top_vectors(double **eigenvectors, int prot_len, int ntop, char *pdbname){

  int i,j,mu;
  char filename[200];
  FILE *fp;

  for(i=0; i < ntop; i++){
    if (i >= (3*prot_len)) break;
    sprintf(filename,"%s_eigenvector_%d.dat",pdbname,i);
    fp =open_file_w(filename);
    for(j=0; j < prot_len; j++){
      fprintf(fp,"%4d ",i);
      for(mu=0; mu < 3; mu++){
        fprintf(fp,"%lf ",eigenvectors[3*prot_len-1-i][j+mu*prot_len]);
      }
      fprintf(fp,"\n");
    }
    fclose(fp);
    printf("Slowest eigenvector number %4d written to file %s\n",i,filename);

  }


}
/******************************************/
void dump_full_covariance_matrix(double **invm, int prot_len, char *pdbname){

  int i,j;
  int mu,nu;
  char filename[200];
  FILE *fp;

  sprintf(filename,"%s_full_cov_matrix.dat",pdbname);
  fp =open_file_w(filename);
  
  for (mu=0; mu < 3; mu++){
    for (nu=0; nu < 3; nu++){

      for(i=0; i < prot_len; i++){
        for(j=0; j < prot_len; j++){
          fprintf(fp,"%4d %4d %4d %4d %e\n",i,j,mu,nu,invm[i+prot_len*mu][j+prot_len*nu]);
        }
      }
    }
  }

  fclose(fp);

  printf("Full covariance matrix written to file %s\n",filename);

}

/******************************************/
void dump_reduced_covariance_matrix(double **invm, int prot_len, char *pdbname){

  int i,j, mu;
  double temp;
  char filename[200];
  FILE *fp;

  sprintf(filename,"%s_reduced_cov_matrix.dat",pdbname);
  
  fp =open_file_w(filename);
  for(i=0; i < prot_len; i++){
    for(j=0; j < prot_len; j++){
      temp=0;
      for (mu=0; mu < 3; mu++){
        temp += invm[i+prot_len*mu][j+prot_len*mu];
      }
      fprintf(fp,"%4d %4d %e\n",i,j,temp);
    }
  }

  fclose(fp);
  printf("Reduced covariance matrix written to file %s\n",filename);

}

/******************************************/
void dump_normalised_reduced_covariance_matrix(double **invm, int prot_len, char *pdbname){

  int i,j,mu;
  double temp, norm1, norm2;
  char filename[200];
  FILE *fp;
  
  sprintf(filename,"%s_normalised_reduced_cov_matrix.dat",pdbname);
  fp =open_file_w(filename);
  for(i=0; i < prot_len; i++){
    for(j=0; j < prot_len; j++){
      
      temp=0.0;
      norm1=0.0;
      norm2=0.0;
      for (mu=0; mu < 3; mu++){
        temp += invm[i+prot_len*mu][j+prot_len*mu];
        norm1 += invm[i+prot_len*mu][i+prot_len*mu];
        norm2 += invm[j+prot_len*mu][j+prot_len*mu];
      }
      fprintf(fp,"%4d %4d %e\n",i,j,temp/(sqrt(norm1*norm2)));
    }
  }

  fclose(fp);


  printf("Normalised reduced covariance matrix written to file %s\n",filename);

}

/******************************************/
void dump_mean_square_displacements(double **invm, int prot_len, char *pdbname){

  int i,mu;
  double temp;
  char filename[200];
  FILE *fp;

  sprintf(filename,"%s_mean_square_displ.dat",pdbname);
  fp =open_file_w(filename);
  for(i=0; i < prot_len; i++){
    temp=0.0;
    for (mu=0; mu < 3; mu++){
      temp += invm[i+prot_len*mu][i+prot_len*mu];
    }
    fprintf(fp,"%4d %e\n",i,temp);
  }
  fclose(fp);
  printf("Residues' mean square displacement written to file %s\n",filename);


}




/******************************************/
