//============================================================================
// Name        : betaGM-lowmem-slepc.cpp
// Author      : Guido Polles
// Version     :
// Copyright   : 
// Description : Hello World in C++, Ansi-style
//============================================================================
#include <mpi.h>

#include <slepceps.h>

#include <cstdio>
#include <iostream>
#include <cmath>
#include <vector>
#include <iomanip>

//extern "C"{
#include "definitions.h"
#include "io.h"
//}

#include "model_with_weighted_interactions.h"
#include "SparseMatrix.h"
#include "SymmetricSparseMatrix.h"
#include "Clock.h"

using namespace std;
using namespace betagm;

static char help[] = "help msg";


#undef __FUNCT__
#define __FUNCT__ "main"



int main(int argc, char* argv[]) {
  Clock clk;
  struct params parameters;

  struct residue residues[MAX_PROT_LEN];
  int prot_len; // number of residues in the protein
  char  pdbfile_name[100], protein_name[100];

  FILE *fp;

  printf("BETA-GAUSSIAN MODEL. (low memory version/ SLEPc) \n\n");

  clk.start("Total");
  read_parameters(&parameters);
  fp = open_file_r("PDB_FILES.TXT");
  fscanf(fp,"%s",protein_name);
  fclose(fp);

  sprintf(pdbfile_name, "%s", protein_name);
  printf("\t**** PROCESSING FILE %s ****\n\n",pdbfile_name);
  read_pdb_file(pdbfile_name,residues ,&prot_len);

  /* check again the number of eigenvectors to be printed  */
  if (parameters.N_SLOWEST_EIGENVECT < 1) parameters.N_SLOWEST_EIGENVECT = 3*prot_len;
  if (parameters.N_SLOWEST_EIGENVECT > 3*prot_len) 
  {
    parameters.N_SLOWEST_EIGENVECT = 3*prot_len;
    printf("\tWARNING: N_SLOWEST_EIGENVECT OUT OF RANGE. ALL EIGENVECTORS WILL BE PRINTED\n");
  }

  // Construct the CB's for all sites except for chain ends and GLY's .
  construct_cbetas(residues, prot_len);
  SparseMatrix interactionMatrix(prot_len*3);

  clk.start("Matrix");
  printf("Constructing interaction matrix\n");
  construct_Matrix_M(interactionMatrix,residues,prot_len,&parameters);
  cout << "\n> Matrix size:" << interactionMatrix.getNumNonZero()/1024/1024*12 << "MB" << endl<< flush;
  clk.stop("Matrix");




  PetscInt dim = interactionMatrix.getSize();
  PetscInt max_row_len;
  PetscInt row_len[dim];
  for (size_t i = 0; i < interactionMatrix.getSize(); ++i){
    row_len[i] = interactionMatrix.getColumn(i).size();
    max_row_len = max(row_len[i],max_row_len);
  }


  clk.start("Eigensolver");
  EPS eps;
  Mat A;
  Vec eigen_vec_re, eigen_vec_im;
  PetscScalar eigen_val_re, eigen_val_im;
  PetscInt nconv,its;
  PetscInt nev = parameters.N_SLOWEST_EIGENVECT;
  PetscInt ncv = nev*3;
  PetscInt mpd = 3000;
  PetscErrorCode ierr;

  cout << "\n\n Call to Eigensolver:" << endl<< flush;

  cout << "> Initializing..." << endl<< flush;
  ierr = SlepcInitialize(&argc,&argv,(char*)0,help);CHKERRQ(ierr);

  cout << "> Preparing Matrix..." << endl<< flush;
  ierr = MatCreateSeqAIJ(PETSC_COMM_WORLD,dim,dim,max_row_len,row_len,&A);CHKERRQ(ierr);
  for (size_t i = 0; i < interactionMatrix.getSize(); ++i){
    for (size_t j = 0; j < interactionMatrix.getColumn(i).size(); ++j){
      PetscInt jidx = interactionMatrix.getColumn(i).at(j).rowIndex;
      PetscScalar val = interactionMatrix.getColumn(i).at(j).value;
      MatSetValue(A,i,jidx,val,INSERT_VALUES);
    }
  }

  ierr = MatAssemblyBegin(A,MAT_FINAL_ASSEMBLY);CHKERRQ(ierr);
  ierr = MatAssemblyEnd(A,MAT_FINAL_ASSEMBLY);CHKERRQ(ierr);
  ierr = MatGetVecs(A,0,&eigen_vec_im);CHKERRQ(ierr);
  ierr = MatGetVecs(A,0,&eigen_vec_re);CHKERRQ(ierr);



  cout << "> Preparing eigenproblem..." << endl<< flush;
  ierr = EPSCreate( PETSC_COMM_WORLD, &eps );CHKERRQ(ierr);
  ierr = EPSSetOperators( eps, A, 0 );CHKERRQ(ierr);
  ierr = EPSSetProblemType( eps, EPS_HEP );CHKERRQ(ierr);
  ierr = EPSSetDimensions( eps, nev, ncv, mpd);CHKERRQ(ierr);
  ierr = EPSSetType( eps, EPSKRYLOVSCHUR );CHKERRQ(ierr);
//  ierr = EPSSetWhichEigenpairs( eps, EPS_SMALLEST_MAGNITUDE);CHKERRQ(ierr);
  ierr = EPSSetWhichEigenpairs( eps, EPS_TARGET_MAGNITUDE);CHKERRQ(ierr);
  ST st;
  EPSGetST(eps,&st);
  STSetType(st, STSINVERT);
  EPSSetTarget(eps, -0.0001);


  ierr = EPSSetFromOptions( eps );CHKERRQ(ierr);
  cout << "> Solving eigenproblem..." << endl<< flush;
  ierr = EPSMonitorSet(eps,EPSMonitorFirst,0,0);CHKERRQ(ierr);
  ierr = EPSSolve( eps );CHKERRQ(ierr);
  cout << "Done." << endl<< flush;
  ierr = EPSGetConverged( eps, &nconv );CHKERRQ(ierr);
  ierr = EPSGetIterationNumber( eps, &its );CHKERRQ(ierr);

  cout << "   #### First 15 eigenvalues ####   "<< endl;
  cout << nconv << " converged eigenvectors in " << its << " iterations." << endl<< flush;
  cout << "\t" << setw(3) << "#" << setw(15) << "Re(l)" << setw(15) << "Im(l)"
       << setw(15) << "Residual" << endl<< flush;


//   system("if [ ! -d eigenspace ]; then mkdir eigenspace; fi");
  
  char command[100];
  sprintf(command,"mkdir results_%s",protein_name);
  if ( system(command) != 0) {
    printf("ERROR! cannot create directory results_%s/.\n",protein_name);
    exit (1);
  }
  
  char evalfname[256];
  sprintf(evalfname,"results_%s/eigenvalues.dat",protein_name);
  FILE* evalf = fopen(evalfname,"w");

  for (PetscInt j = 0; j < nconv; ++j) {
    PetscReal res_norm;
    ierr = EPSGetEigenpair( eps, j,&eigen_val_re,&eigen_val_im,eigen_vec_re, eigen_vec_im);CHKERRQ(ierr);
    ierr = EPSComputeResidualNorm( eps, j, &res_norm);CHKERRQ(ierr);
    if(j<15) cout << "\t" << setw(3) << j+1 << setw(15) << eigen_val_re
                  << setw(15) << eigen_val_im << setw(15) << res_norm << endl<< flush;

    // output
    fprintf(evalf,"%d %f\n",j,eigen_val_re);

    char evecfname[256];
    PetscScalar* x = (PetscScalar*)malloc(dim);
    VecGetArray(eigen_vec_re,&x);
    sprintf(evecfname,"results_%s/eigenvector_%ld.dat",protein_name,j);
    FILE* evecf = fopen(evecfname,"w");
    for (PetscInt k = 0; k < dim/3; ++k) {
      fprintf(evecf,"%d %f %f %f\n",k,x[k],
                                      x[dim/3+k],
                                      x[dim/3*2+k]);
    }
    fclose(evecf);
  }


  // destroy workspace
  ierr = EPSDestroy(&eps);CHKERRQ(ierr);
  ierr = MatDestroy(&A);CHKERRQ(ierr);
  ierr = VecDestroy(&eigen_vec_im);CHKERRQ(ierr);
  ierr = VecDestroy(&eigen_vec_re);CHKERRQ(ierr);
  ierr = SlepcFinalize();

  cout << endl <<" Timings:" << endl<< flush;
  clk.print("Matrix");
  clk.print("Eigensolver");
  clk.print("Total");
  return 0;



  return 0;
}
