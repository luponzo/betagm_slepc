/*
 * Clock.cpp
 *
 *  Created on: May 27, 2013
 *      Author: gpolles
 */

#include <iostream>
#include <iomanip>
#include <ctime>
#include "Clock.h"

#ifndef NULL
#define NULL 0
#endif


using namespace std;

Clock::Clock() {}

Clock::~Clock() {}

void Clock::start(const std::string& label) {
	int idx = find(label);
	if( idx != -1){
		_clocks[idx].running = true;
		_clocks[idx].start = time(NULL);
	}else{
		clk_t clk;
		clk.running = true;
		clk.label = label;
		clk.end = 0;
		_clocks.push_back(clk);
		_clocks.back().start = time(NULL);
	}
}

void Clock::stop(const std::string& label) {
	int idx = find(label);
	if( idx != -1){
		_clocks[idx].end = time(NULL);
		_clocks[idx].running = false;
	}else{
		std::cerr << "Clock::stop() error. No clock labeled \""
				  << label << "\"" << std::endl;
	}
}

void Clock::print(const std::string& label, std::ostream& stream) {
	int idx = find(label);

	if( idx != -1){
		long int elapsed = _clocks[idx].running ? time(NULL) - _clocks[idx].start:
				_clocks[idx].end - _clocks[idx].start;
		int seconds = elapsed % 60;
		elapsed/=60;
		int minutes = elapsed % 60;
		elapsed/=60;
		int hours = elapsed % 24;
		elapsed/=24;
		int days = elapsed;
		stream << "Timing for " << _clocks[idx].label << ": "
			   << setw(3) << days << " d "
			   << setw(3) << hours << " h "
			   << setw(3) << minutes << " m "
			   << setw(3) << seconds << " s " << endl;
	}else{
		std::cerr << "Clock::print() error. No clock labeled \""
				<< label << "\"" << std::endl;
	}
}

long int Clock::getTime(const std::string& label) {
	int idx = find(label);
	if( idx != -1){
		return _clocks[idx].running ? time(NULL) - _clocks[idx].start:
				_clocks[idx].end - _clocks[idx].start;
	}else{
		std::cerr << "Clock::getTime() error. No clock labeled \""
						  << label << "\"" << std::endl;
		return -1;
	}
}


int Clock::find(const std::string& label) const {
	int index = -1;
	for (int i = 0; i < int(_clocks.size()); ++i) {
		if(label==_clocks[i].label){
			index = i;
			break;
		}
	}
	return index;
}




